# Examen Parcial - Programación III
### Ing. y Lic. en Sistemas (Junio 2022)

### Objetivos
- Desarrollar endpoints que permitan el registro y consulta de la entidad Entrada a Cine
### Tiempo
- 2 horas reloj
### Evaluacion
- Se evaluará la versión del proyecto en el repositorio correspondiente a la hora de finalización del examen, estimada para el día 22/06/2022 20:00
- El proyecto debe compilar sin errores en cualquier entorno de programacion en el que se abra
- Todos los test unitarios deben pasar en verde
- El proyecto contempla nivel Core y Adapter (BD y API Rest)

## Consigna
#### Módulo Entradas
_Se desea implementar un backend para un microservicio que permita registrar entradas de cine._

![ProgramacionIII.drawio.png](./ProgramacionIII.drawio.png)

#### Restricciones:
- No puede existir dos entradas con el mismo numero de ticket
- Todos los atributos de entrada son obligatorios
- El precio no puede ser negativo
- La fecha no puede ser superior a la actual

#### Funcionalidad
- Crear Entrada
  - Endpoint: POST http://localhost:8080/entrada
  - RequestBody:
    ```json
    {
      "numero_ticket": "12345",
      "pelicula": "Avengers",
      "fecha_hora":"2022-06-22T18:00:00.000Z",
      "precio": 300.00
    }
    ```

#### Buenas prácticas y conceptos a considerar
- La nomenclatura de paquetes será en minúsculas
- La nomenclatura de clases será en UpperCamelCase
- La nomenclatura de métodos será en lowerCamelCase
- La organización de paquetes será por modelo->aspecto, tanto a nivel src/main como a nivel src/test. Ejemplo:
  ```
  entrada
  └─ excepciones
  └─ modelo
  └─ repositorio
  └─ casodeuso
  ```
- Se deben crear pruebas unitarias en Core. No es requerido en adapter, pero se contemplará el valor agregado si se incorporan
- Usar Excepciones personalizadas
- Se debe usar método factory/instancia para crear objetos
- Nomenclatura representativa de clases, métodos, etc.
